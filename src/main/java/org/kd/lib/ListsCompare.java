package org.kd.lib;

import org.kd.model.board.BoardField;

import java.util.Comparator;
import java.util.List;

public class ListsCompare {

    public static boolean compareBoardFieldLists(List<BoardField> expected, List<BoardField> actual) {

        expected.sort(Comparator.comparing(BoardField::getBoardXCoord).thenComparing(BoardField::getBoardYCoord));
        actual.sort(Comparator.comparing(BoardField::getBoardXCoord).thenComparing(BoardField::getBoardYCoord));

        if (expected.size() != actual.size())
            return false;
        else {
            for (int i = 0; i < expected.size(); i++)
                if (!expected.get(i).equals(actual.get(i)))
                    return false;
        }
        return true;
    }
}
