package org.kd.lib;

public class Sout {

    public static void print(char c, int times){
        while (times > 0){
            System.out.print(c);
            times--;
        }
    }

    public static void println(char c, int times){
        print(c, times);
        System.out.println();
    }
}
