package org.kd.model;

public interface IMoveInput {

    Character readPromotion();

    String readMove();

    boolean getMoveUpdated();

    void resetMoveUpdated();
}
