package org.kd.model;

public enum Side {
    WHITE, BLACK, NONE;

    public static Side changeTurn(Side side) {
        if (side.equals(WHITE)) {
            return BLACK;
        } else return WHITE;
    }

    public static Side getReverse(Side side) {
        return (side.equals(WHITE)) ? BLACK : WHITE;
    }
}
