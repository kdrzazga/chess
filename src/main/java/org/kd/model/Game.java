package org.kd.model;

import org.kd.controller.InputParser;
import org.kd.model.board.Board;
import org.kd.model.board.BoardFactory;

import java.text.ParseException;

public class Game extends Thread {

    private Board gameBoard;
    private final IDrawable output;
    private final IMoveInput moveInput;
    private final InputParser moveParser;

    public Game(IDrawable gameOutput, IMoveInput moveInput) {
        this.output = gameOutput;
        this.moveInput = moveInput;
        this.moveParser = new InputParser();
    }

    public void start() {
        gameBoard = BoardFactory.createStartingBoard();
        gameBoard.setGameRunning(true);
        output.draw(gameBoard);
        this.run();
    }

    public void finish() {
        gameBoard.setGameRunning(false);
    }

    public void run() {
        while (true) {

            if (gameBoard.isGameRunning()) {

                try {
                    String move = moveInput.readMove();
                    gameBoard.move(moveParser.parseSource(move), moveParser.parseDestination(move));
                    output.draw(gameBoard);
                } catch (ParseException exc) {
                    System.err.println(exc.getMessage());
                }

            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException iexc) {
                System.err.println(iexc.getMessage());
            }
        }
    }

}
