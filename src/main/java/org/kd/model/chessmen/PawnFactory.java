package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import java.util.ArrayList;
import java.util.List;

public class PawnFactory {

    public static List<Pawn> createWhitesStartingRow() {
        return createPawnsRow(2, Side.WHITE);
    }

    public static List<Pawn> createBlacksStartingRow() {
        return createPawnsRow(7, Side.BLACK);
    }

    public static List<Pawn> createPawnsRow(int row, Side side) {
        List<Pawn> startingRow = new ArrayList<>();

        Character columns[] = new Character[]{
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'
        };

        for (char col : columns) {
            startingRow.add(new Pawn(new BoardField(col, row), side));
        }
        return startingRow;
    }
}
