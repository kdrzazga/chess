package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import java.util.HashMap;

public class Chessman {

    protected BoardField field;
    protected Side side;

    Chessman(){
    }

    public Chessman(Chessman chessman){
        this.side = chessman.getSide();
        this.field = chessman.getField();
    }

    public Chessman(BoardField field, Side side){
        this.field = field;
        this.side = side;
    }

    private static final HashMap<Class<? extends Chessman>, Character> chessmanSymbolMap = new HashMap<>(6);
    private static final HashMap<Side, Character> sideMap = new HashMap<>(2);

    static {
        chessmanSymbolMap.put(Pawn.class, 'P');
        chessmanSymbolMap.put(Tower.class, 'T');
        chessmanSymbolMap.put(Knight.class, 'k');
        chessmanSymbolMap.put(Bishop.class, 'B');
        chessmanSymbolMap.put(Queen.class, 'Q');
        chessmanSymbolMap.put(King.class, 'K');
        chessmanSymbolMap.put(NullChessman.class, ' ');//will be trimmed anyway

        sideMap.put(Side.BLACK, 'b');
        sideMap.put(Side.WHITE, 'w');
        sideMap.put(Side.NONE, ' ');//will be trimmed too
    }

    public boolean moveCompliesWithRules(BoardField destField){
        return false;
    }

    public void setField(String field) {
        this.setField(new BoardField(field));
    }

    public void setField(int tableX, int tableY){
        this.setField(new BoardField(tableX, tableY));
    }

    public void setField(BoardField field) {
        this.field = field;
    }

    public BoardField getField(){
        return field;
    }

    public Side getSide(){
        return side;
    }

    public static String getTextSymbol(Chessman chessman){
        return (chessmanSymbolMap.get(chessman.getClass())
                + "" + getTextSymbol(chessman.getSide())).trim();
    }

    public static String getTextSymbol(Class<? extends Chessman> chessmanClass){
        return ("" + chessmanSymbolMap.get(chessmanClass)).trim();
    }

    public static String getTextSymbol(Side side){
        return ("" + sideMap.get(side)).trim();
    }


    public boolean isAlive(){
        return !this.getClass().equals(NullChessman.class);
    }

}
