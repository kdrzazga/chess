package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import static java.lang.Math.abs;

public class Knight extends Chessman {


    public Knight(BoardField field, Side side) {
        super(field, side);
    }

    public Knight(String field, Side side) {
        super(new BoardField(field), side);
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {

        int deltaX = abs(destField.getTableXCoord() - field.getTableXCoord());
        int deltaY = abs(destField.getTableYCoord() - field.getTableYCoord());

        return (deltaX == 2 && deltaY == 1)
                || (deltaX == 1 && deltaY == 2);
    }
}
