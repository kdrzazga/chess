package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import static java.lang.Math.abs;

public class Queen extends Chessman {

    public Queen(Side side) {
        this.side = side;
        if (side.equals(Side.BLACK))
            this.setField("d8");
        else this.setField("d1");
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {
        boolean towerCondition =
                !destField.equals(this.field)
                        && ((destField.getTableXCoord() == this.field.getTableXCoord())
                        || (destField.getTableYCoord() == this.field.getTableYCoord()));

        boolean bishopCondition = abs(destField.getTableXCoord() - field.getTableXCoord())
                == abs(destField.getTableYCoord() - field.getTableYCoord());

        return towerCondition || bishopCondition;
    }
}
