package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

public class Tower extends Chessman {

    public Tower(BoardField field, Side side) {
        super(field, side);
    }

    public Tower(String field, Side side) {
        super(new BoardField(field), side);
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {
        return
                !destField.equals(this.field)
                        && ((destField.getTableXCoord() == this.field.getTableXCoord())
                        || (destField.getTableYCoord() == this.field.getTableYCoord()));
    }
}
