package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import static java.lang.Math.abs;

public class King extends Chessman {

    private boolean alreadyMoved;
    private boolean checked;

    public King(Side side) {
        alreadyMoved = false;
        checked = false;
        this.side = side;
        if (side.equals(Side.BLACK))
            this.setField("e8");
        else this.setField("e1");
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {

        /*
        --------
        |x1|y1|z1|
        |x2|K |z2|
        |x3|y3|z3|

        reshuffle:
         */

        boolean columnXorZ = abs(destField.getTableXCoord() - this.field.getTableXCoord()) <= 1;
        boolean row1or3 = abs(destField.getTableYCoord() - this.field.getTableYCoord()) <= 1;
        boolean reshuffle = !this.alreadyMoved
                && !checked
                && abs(destField.getTableXCoord() - this.field.getTableXCoord()) == 2
                && abs(destField.getTableYCoord() - this.field.getTableYCoord()) == 0;
        return
                !destField.equals(this.field)
                        && (columnXorZ && row1or3)
                        || reshuffle;
    }

    public void setAlreadyMoved() {
        this.alreadyMoved = true;
    }

    public boolean getAlreadyMoved(){
        return this.alreadyMoved;
    }
}
