package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

import static java.lang.Math.abs;

public class Bishop extends Chessman {

    public Bishop(BoardField field, Side side) {
        super(field, side);
    }

    public Bishop(String field, Side side) {
        super(new BoardField(field), side);
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {
        return abs(destField.getTableXCoord() - field.getTableXCoord())
                == abs(destField.getTableYCoord() - field.getTableYCoord());
    }
}
