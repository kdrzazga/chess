package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.BoardField;

public class NullChessman extends Chessman{

    public NullChessman(BoardField field){
        super(field, Side.NONE);
    }

    @Override
    public boolean moveCompliesWithRules(BoardField destField) {
        return true;
    }
}
