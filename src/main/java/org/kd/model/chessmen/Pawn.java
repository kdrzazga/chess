package org.kd.model.chessmen;

import org.kd.model.Side;
import org.kd.model.board.Board;
import org.kd.model.board.BoardField;

import java.awt.*;

public class Pawn extends Chessman {

    private final FieldTranslation[] validFieldsVectors = {
            new FieldTranslation(0, 1),
            new FieldTranslation(-1, 1),
            new FieldTranslation(1, 1)
    };

    public Pawn(String field, Side side){
        this.field = new BoardField(field);
        this.side = side;
    }

    public Pawn(BoardField field, Side side) {
        this.field = field;
        this.side = side;
    }

    public boolean moveCompliesWithRules(BoardField destField) {

        int colorDirectionCoeff = this.getSide().equals(Side.WHITE) ? 1 : -1;//white pawn goes up (+1), black goes down (-1)

        for (FieldTranslation translation : validFieldsVectors) {
            int tableX = field.getTableXCoord() + colorDirectionCoeff * translation.x;
            int tableY = field.getTableYCoord() + colorDirectionCoeff * translation.y;

            if (tableX >= 0 && tableX < Board.COLUMN_SIZE && tableY >= 0 && tableY < Board.ROW_SIZE) {
                BoardField validDestField = new BoardField(tableX, tableY);

                if (validDestField.equals(destField))
                    return true;
            }
        }

        if (this.isOnStartLine()) {
            FieldTranslation translation = new FieldTranslation(0, 2);
            int tableX = field.getTableXCoord() + colorDirectionCoeff * translation.x;
            int tableY = field.getTableYCoord() + colorDirectionCoeff * translation.y;

            if (tableX >= 0 && tableX < Board.COLUMN_SIZE && tableY >= 0 && tableY < Board.ROW_SIZE) {
                BoardField validDestField = new BoardField(tableX, tableY);

                if (validDestField.equals(destField))
                    return true;
            }
        }

        return false;
    }

    private boolean isOnStartLine() {
        return field.isPawnStartLine(this.getSide());
    }

    private class FieldTranslation extends Point {
        FieldTranslation(int x, int y) {
            super(x, y);
        }
    }
}
