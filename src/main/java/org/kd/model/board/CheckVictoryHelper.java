package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.King;

public class CheckVictoryHelper {

    private GameState state = GameState.PLAYING;

    public CheckVictoryHelper(){
    }

    public CheckVictoryHelper(CheckVictoryHelper helper){
        this.state = helper.getState();
    }

    public void checkVictory(Board board){
    }

    private boolean checkStaleMate(Board board, Side side){
        King king = (King)board.getActiveChessmanAt(
                board.getChessmenPositions(King.class, side).get(0));

        throw new RuntimeException("not implemented yet");
        //AfterMoveAnalyzer.canKingBeBeaten()

    }

    public void setState(GameState state){
        this.state = state;
    }

    public GameState getState() {
        return state;
    }

}
