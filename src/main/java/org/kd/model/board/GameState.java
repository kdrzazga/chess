package org.kd.model.board;

public enum GameState{
    PLAYING, WHITE_WIN, BLACK_WIN, STALEMATE
}