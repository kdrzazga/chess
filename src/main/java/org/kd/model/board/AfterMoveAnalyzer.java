package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.King;

import java.util.List;

class AfterMoveAnalyzer {

    public static boolean canKingBeBeaten(Board board, BoardField src, BoardField dest) {
        Side turnToBeKeeped = board.getTurn();

        board.move(src, dest);//this will change turn (or not, if the move isn't correct, so
        if (board.getTurn() != turnToBeKeeped)
            board.changeTurn();//change it back

        MoveConditionsHelper moveConditionsHelper = new MoveConditionsHelper(board);

        List<BoardField> ourKingsLocations = board.getChessmenPositions(King.class, board.getTurn());

        final BoardField kingPosition = ourKingsLocations.get(0);

        Side opponentSide = Side.getReverse(board.getTurn());

        return board.getAllChessmen(opponentSide)
                .stream()
                .anyMatch(opponent -> moveConditionsHelper.moveConditionsMet(kingPosition, opponent, opponentSide));
    }
}
