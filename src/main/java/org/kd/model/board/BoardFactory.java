package org.kd.model.board;

public class BoardFactory {

    BoardFactory(){
    }

    public static Board createStartingBoard(){
        return  new BoardBuilder().addWhitePawns().addWhiteStartingRow1().addBlackPawns().addBlackStartingRow8().build();
    }
}
