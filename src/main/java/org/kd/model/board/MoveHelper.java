package org.kd.model.board;

import org.kd.model.chessmen.Chessman;
import org.kd.model.chessmen.King;
import org.kd.model.chessmen.NullChessman;

import java.util.List;

import static java.lang.Math.abs;

public class MoveHelper {

    private final Board board;

    public MoveHelper(Board board) {
        this.board = board;
    }

    public List<Chessman> performMove(BoardField src, BoardField dest, Chessman chessman) {
        if (checkCastlingForKing(chessman, dest))
            ;//performCastling((King)chessman, dest);

        checkBeat(chessman, dest);
        chessman.setField(dest);
        if (chessman.getClass().equals(King.class))
            ((King) chessman).setAlreadyMoved();

        addMoveToHistory(chessman, src, dest);

        checkPawnPromotion(chessman);

        return this.board.chessmen;
    }

    private void performCastling(King king, BoardField dest) {

        System.err.println(Board.class.getSimpleName() + " - Castling not implemented yet");

    }

    private void addMoveToHistory(Chessman chessman, BoardField src, BoardField dest) {
        board.movesHistory.add(
                chessman.getSide()
                        + " " + new ChessMove(src, dest).toString());
    }

    private boolean checkCastlingForKing(Chessman chessman, BoardField dest) {
        if (!chessman.getClass().equals(King.class))
            return true;
        else {
            King king = (King) chessman;
            boolean horizontalMove = dest.getBoardXCoord() == king.getField().getBoardXCoord();
            boolean twoFieldsMove = abs(dest.getBoardYCoord() - king.getField().getBoardYCoord()) == 2;
            boolean noCheck = true;
            boolean noCheckAlongTheWay = true;
            boolean correctTowerPosition = true;

            System.err.println(Board.class.getSimpleName() + " checkCastlingForKing UNFINISHED");

            return !king.getAlreadyMoved() && horizontalMove && twoFieldsMove && noCheck && noCheckAlongTheWay && correctTowerPosition;
        }
    }

    private void checkBeat(Chessman chessman, BoardField dest) {
        Chessman targetChessman = board.getActiveChessmanAt(dest);

        if (!targetChessman.getClass().equals(NullChessman.class))
            kill(dest);
    }

    private void checkPawnPromotion(Chessman chessman) {

    }

    private void kill(BoardField orphanField) {

        for (int i = 0; i < board.chessmen.size(); i++) {
            if (orphanField.equals(board.chessmen.get(i).getField())) {
                board.beatenChessmen.add(board.chessmen.remove(i));
                break;
            }
        }
    }

    public List<Chessman> getBeatenChessmen(){
        return board.beatenChessmen;
    }

    private class ChessMove {
        BoardField src;
        BoardField dest;

        ChessMove(BoardField source, BoardField destination) {
            this.src = source;
            this.dest = destination;
        }

        @Override
        public String toString() {
            return src.toString().concat(" -> ").concat(dest.toString());
        }
    }

}
