package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.Chessman;
import org.kd.model.chessmen.King;
import org.kd.model.chessmen.NullChessman;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

public class Board {

    public static final int ROW_SIZE = 8;
    public static final int COLUMN_SIZE = 8;

    private final MoveConditionsHelper moveConditionsHelper = new MoveConditionsHelper(this);

    public final List<Chessman> chessmen = new ArrayList<>(4 * COLUMN_SIZE);
    public final List<Chessman> beatenChessmen = new ArrayList<>(4 * COLUMN_SIZE);
    public final List<String> movesHistory = new ArrayList<>();

    private boolean gameRunning = false;
    private Side turn = Side.WHITE;

    public Board() {
    }

    public Board(Board boardToCopy) {
        this.chessmen.addAll(boardToCopy.chessmen);
        this.beatenChessmen.addAll(boardToCopy.beatenChessmen);
        this.movesHistory.addAll(boardToCopy.movesHistory);
        this.gameRunning = boardToCopy.gameRunning;
        this.turn = boardToCopy.turn;
    }

    public boolean isGameRunning() {
        return this.gameRunning;
    }

    public void setGameRunning(boolean gameRunning) {
        this.gameRunning = gameRunning;
    }

    public void move(String src, String dest) {
        move(new BoardField(src), new BoardField(dest));
    }

    public void changeTurn() {
        this.turn = Side.changeTurn(this.getTurn());
    }

    public Chessman getActiveChessmanAt(int x, int y) {
        return getActiveChessmanAt(new BoardField(x, y));
    }

    public Chessman getActiveChessmanAt(String field) {
        return getActiveChessmanAt(new BoardField(field));
    }

    public Chessman getActiveChessmanAt(BoardField position) {
        for (Chessman chessman : chessmen) {
            if (chessman.getField().equals(position)
                    && chessman.isAlive())
                return chessman;
        }
        return new NullChessman(position);
    }

    public List<Chessman> getAllChessmen(Side side) {
        return this.chessmen.stream()
                .filter(chessman -> chessman.getSide().equals(side))
                .collect(Collectors.toList());
    }

    public List<BoardField> getChessmenPositions(Class<? extends Chessman> chessmanClass, Side side) {

        return chessmen.stream()
                .filter(chessman -> chessman.getSide().equals(side))
                .filter(chessman -> chessman.getClass().equals(chessmanClass))
                .map(Chessman::getField)
                .collect(Collectors.toList());
    }

    public List<BoardField> getAllUnoccupiedFields() {
        List<BoardField> vacantFields = new ArrayList<>(64);
        for (int col = 0; col < Board.COLUMN_SIZE; col++)
            for (int row = 0; row < Board.ROW_SIZE; row++) {
                final BoardField position = new BoardField(row, col);
                if (getActiveChessmanAt(position).getClass().equals(NullChessman.class))
                    vacantFields.add(position);
            }
        return vacantFields;
    }

    public void move(BoardField src, BoardField dest) {

        Chessman chessman = getActiveChessmanAt(src);

        if (moveConditionsHelper.moveConditionsMet(dest, chessman, turn)) {

            if (checkCastlingForKing(chessman, dest))
                ;//performCastling((King)chessman, dest);

            checkBeat(chessman, dest);
            chessman.setField(dest);
            if (chessman.getClass().equals(King.class))
                ((King) chessman).setAlreadyMoved();

            addMoveToHistory(chessman, src, dest);
            checkVictory();
            checkPawnPromotion(chessman);
            this.changeTurn();
        }

    }

    private void performCastling(King king, BoardField dest) {

        System.err.println(Board.class.getSimpleName() + " - Castling not implemented yet");

    }

    private void addMoveToHistory(Chessman chessman, BoardField src, BoardField dest) {
        this.movesHistory.add(
                chessman.getSide()
                        + " " + new ChessMove(src, dest).toString());
    }

    private boolean checkCastlingForKing(Chessman chessman, BoardField dest) {
        if (!chessman.getClass().equals(King.class))
            return true;
        else {
            King king = (King) chessman;
            boolean horizontalMove = dest.getBoardXCoord() == king.getField().getBoardXCoord();
            boolean twoFieldsMove = abs(dest.getBoardYCoord() - king.getField().getBoardYCoord()) == 2;
            boolean noCheck = true;
            boolean noCheckAlongTheWay = true;
            boolean correctTowerPosition = true;

            System.err.println(Board.class.getSimpleName() + " checkCastlingForKing UNFINISHED");

            return !king.getAlreadyMoved() && horizontalMove && twoFieldsMove && noCheck && noCheckAlongTheWay && correctTowerPosition;
        }
    }


    private void checkBeat(Chessman chessman, BoardField dest) {
        Chessman targetChessman = getActiveChessmanAt(dest);

        if (!targetChessman.getClass().equals(NullChessman.class))
            kill(dest);
    }

    private void checkVictory() {

    }

    private void checkPawnPromotion(Chessman chessman) {

    }

    private void kill(BoardField orphanField) {

        for (int i = 0; i < chessmen.size(); i++) {
            if (orphanField.equals(chessmen.get(i).getField())) {
                beatenChessmen.add(chessmen.remove(i));
                break;
            }
        }
    }

    public Side getTurn() {
        return this.turn;
    }

    private class ChessMove {
        BoardField src;
        BoardField dest;

        ChessMove(BoardField source, BoardField destination) {
            this.src = source;
            this.dest = destination;
        }

        @Override
        public String toString() {
            return src.toString().concat(" -> ").concat(dest.toString());
        }
    }
}
