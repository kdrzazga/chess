package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.*;

import java.util.ArrayList;
import java.util.List;

public class BoardBuilder {

    private static Board board;

    public BoardBuilder(){
        board = new Board();
    }

    public BoardBuilder addBlackStartingRow8(){
        List<Chessman> chessmen = new ArrayList<>(8);
        chessmen.add(new Tower("a8", Side.BLACK));
        chessmen.add(new Knight("b8", Side.BLACK));
        chessmen.add(new Bishop("c8", Side.BLACK));
        chessmen.add(new Queen(Side.BLACK));
        chessmen.add(new King(Side.BLACK));
        chessmen.add(new Bishop("f8", Side.BLACK));
        chessmen.add(new Knight("g8", Side.BLACK));
        chessmen.add(new Tower("h8", Side.BLACK));

        board.chessmen.addAll(chessmen);
        return this;
    }

    public BoardBuilder addWhiteStartingRow1(){
        List<Chessman> chessmen = new ArrayList<>(8);
        chessmen.add(new Tower("a1", Side.WHITE));
        chessmen.add(new Knight("b1", Side.WHITE));
        chessmen.add(new Bishop("c1", Side.WHITE));
        chessmen.add(new Queen(Side.WHITE));
        chessmen.add(new King(Side.WHITE));
        chessmen.add(new Bishop("f1", Side.WHITE));
        chessmen.add(new Knight("g1", Side.WHITE));
        chessmen.add(new Tower("h1", Side.WHITE));

        board.chessmen.addAll(chessmen);
        return this;
    }

    public BoardBuilder addWhitePawns(){
        board.chessmen.addAll(PawnFactory.createWhitesStartingRow());
        return this;
    }

    public BoardBuilder addBlackPawns(){
        board.chessmen.addAll(PawnFactory.createBlacksStartingRow());
        return this;
    }

    public Board build(){
        return board;
    }
}
