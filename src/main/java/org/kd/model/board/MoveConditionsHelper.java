package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.*;

import java.util.stream.IntStream;

import static java.lang.Integer.max;
import static java.lang.Integer.min;
import static java.lang.Math.abs;

public class MoveConditionsHelper {
    private final Board board;
    private static final boolean DEBUG = true;

    public MoveConditionsHelper(Board board) {
        this.board = board;
    }

    public boolean moveConditionsMet(BoardField dest, Chessman chessman, Side turn) {
        return chessman.isAlive()
                && chessman.getSide().equals(turn)
                && chessman.moveCompliesWithRules(dest)
                && noFriendlyBeat(dest, chessman)
                && noObstacles(chessman, dest)
                && !kingIsNotChecked(chessman, dest)
                && checkSlashBeatForPawn(chessman, dest)
                && !pawnTriesBeatForward(chessman, dest);
    }

    private boolean noFriendlyBeat(BoardField dest, Chessman chessman) {
        return !board.getActiveChessmanAt(dest).getSide().equals(chessman.getSide());
    }

    boolean noObstacles(Chessman chessman, BoardField dest) {

        final int xSrc = chessman.getField().getTableXCoord();
        final int ySrc = chessman.getField().getTableYCoord();
        final int xDst = dest.getTableXCoord();
        final int yDst = dest.getTableYCoord();
        final String DEBUG_MSG = "DEBUG " + Board.class.getSimpleName() + ": ";
        final String DEBUG_MOVE_MSG = "Move " + xSrc + "," + ySrc + "->"
                + xDst + "," + yDst + " "
                + new BoardField(xSrc, ySrc) + "->" + new BoardField(xDst, yDst);

        if (chessman.getClass().equals(Knight.class)) {
            return true;

        } else {
            if (xSrc == xDst) {
                if (IntStream.range(min(ySrc, yDst) + 1, max(ySrc, yDst))
                        .anyMatch(y -> board.getActiveChessmanAt(xSrc, y).isAlive()))
                    return false;

            } else if (ySrc == yDst) {
                if (IntStream.range(min(xSrc, xDst) + 1, max(xSrc, xDst))
                        .anyMatch(x -> board.getActiveChessmanAt(x, ySrc).isAlive()))
                    return false;

            } else if (abs(xDst - xSrc) == abs(yDst - ySrc)) {
                boolean backslashMove = xSrc < xDst && ySrc > yDst || xSrc > xDst && ySrc < yDst;
                final int minX = min(xSrc, xDst) + 1;
                final int maxX = max(xSrc, xDst);
                if (backslashMove) {

                    final int maxY = max(ySrc, yDst) - 1;

                    if (IntStream.range(0, maxX - minX)
                            .anyMatch(distance -> board.getActiveChessmanAt(minX + distance, maxY + distance).isAlive())) {
                        return false;
                    }
                } else { //slash move
                    /*final int minY = min(ySrc, yDst);

                    if (IntStream.range(0, maxX - minX)
                            .anyMatch(distance -> board.getActiveChessmanAt(minX + distance, minY + distance).isAlive())) {
                        return false;
                    }*/

                    int y = min(ySrc, yDst) + 1;
                    for (int x = min(xSrc, xDst) + 1; x < max(xSrc, xDst); x++) {
                        if (DEBUG) {
                            System.out.println(DEBUG_MSG + " : Field "
                                    + x + "," + y + " " + new BoardField(x, y)
                                    + " " + board.getActiveChessmanAt(x, y).getClass().getSimpleName());
                        }

                        if (board.getActiveChessmanAt(x, y).isAlive()) {
                            if (DEBUG)
                                System.out.println("\n" + DEBUG_MSG + DEBUG_MOVE_MSG
                                        + " NOT accepted");
                            return false;
                        }
                        y++;
                    }
                }
            } else
                throw new RuntimeException(
                        "Method cannot be called for chessman src = " + xSrc + " ," + ySrc
                                + " dst = " + xDst + " ," + yDst);
        }
        if (DEBUG)
            System.out.println("\n" + DEBUG_MSG + DEBUG_MOVE_MSG + " accepted");
        return true;
    }

    private boolean kingIsNotChecked(Chessman chessman, BoardField dest) {
        if (chessman.getClass().equals(King.class)) {
            King king = (King) chessman;
            return false;//during the check King can (and must) move
        } else {

            System.err.println("kingIsNotChecked - not implemented yet");
            return false;

        }
    }

    private boolean isFieldSafeFromCheck(BoardField field, Side opponentSide) {
        return board.chessmen.stream()
                .filter(chessman -> chessman.getSide().equals(opponentSide))
                .noneMatch(opponent -> noObstacles(opponent, field));
    }

    private boolean checkSlashBeatForPawn(Chessman chessman, BoardField dest) {
        boolean notPawn = !chessman.getClass().equals(Pawn.class);
        boolean notSlashBeat = dest.getBoardXCoord() == chessman.getField().getBoardXCoord();

        return notPawn
                || notSlashBeat
                || (!board.getActiveChessmanAt(dest).getClass().equals(NullChessman.class)
                && !board.getActiveChessmanAt(dest).getSide().equals(chessman.getSide()));
    }

    private boolean pawnTriesBeatForward(Chessman chessman, BoardField dest) {

        if (!chessman.getClass().equals(Pawn.class))
            return false;
        else {
            Pawn pawn = (Pawn) chessman;
            boolean pawnTriesMoveForward = pawn.getField().getBoardXCoord() == dest.getBoardXCoord();
            boolean destinationOccupied = board.getActiveChessmanAt(dest).isAlive();
            return pawnTriesMoveForward && destinationOccupied;
        }
    }
}