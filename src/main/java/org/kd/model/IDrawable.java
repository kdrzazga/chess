package org.kd.model;

import org.kd.model.board.Board;

public interface IDrawable {

    void draw(Board board);
}
