package org.kd.controller;

import org.kd.model.board.BoardField;
import org.kd.model.chessmen.*;

import java.text.ParseException;
import java.util.HashMap;

public class InputParser {

    private final static HashMap<Character, Class<? extends Chessman>> promotionSymbolMap = new HashMap<>(4);

    static {
        promotionSymbolMap.put('T', Tower.class);
        promotionSymbolMap.put('K', Knight.class);
        promotionSymbolMap.put('B', Bishop.class);
        promotionSymbolMap.put('Q', Queen.class);
    }

    public static boolean isSymbolCorrect(char symbol){
        return promotionSymbolMap.containsKey(("" + symbol).toUpperCase().charAt(0));
    }

    public Class<? extends Chessman> parsePromotion(char chessmanSymbol) {
        chessmanSymbol = ("" + chessmanSymbol).toUpperCase().charAt(0);

        return promotionSymbolMap.getOrDefault(chessmanSymbol, Queen.class);//default = Queen
    }

    public String getPromotionPossibilities() {
        String promoPossibilities = "";
        for (Character key : promotionSymbolMap.keySet()) {
            promoPossibilities
                    .concat("" + key)
                    .concat(" - ")
                    .concat(promotionSymbolMap.get(key).getSimpleName()
                    .concat("  "));
        }

        return promoPossibilities.trim();
    }

    public BoardField parseSource(String userMove) throws ParseException {
        userMove = userMove.toLowerCase();
        if ("exit".equals(userMove))
            System.exit(0);
        validate(userMove);
        return
                new BoardField(userMove.substring(0, 2));
    }

    public BoardField parseDestination(String userMove) throws ParseException {
        userMove = userMove.toLowerCase();
        validate(userMove);
        return new BoardField(userMove.substring(2));
    }

    private void validate(String userMove) throws ParseException {
        boolean validated =
                (userMove.length() == 4)
                        && (BoardField.availableXs.contains("" + userMove.charAt(0)))
                        && (Character.isDigit(userMove.charAt(1)))
                        && (BoardField.availableXs.contains("" + userMove.charAt(2)))
                        && (Character.isDigit(userMove.charAt(3)));

        if (!validated)
            throw new ParseException(userMove + " is not valid chess move", 0);

    }
}
