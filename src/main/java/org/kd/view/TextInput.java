package org.kd.view;

import org.kd.controller.InputParser;
import org.kd.model.IMoveInput;
import java.util.Scanner;

public class TextInput implements IMoveInput {

    private boolean moveUpdated = true;

    @Override
    public Character readPromotion() {
        Scanner scanner = new Scanner(System.in);
        Character chessmanSymbol = ' ';
        while (InputParser.isSymbolCorrect(chessmanSymbol)){
            chessmanSymbol = scanner.nextLine().charAt(0);
        }
        return chessmanSymbol;
    }

    @Override
    public String readMove() {
        Scanner scanner = new Scanner(System.in);
        String move = "";
        final int expectedLength = 4;
        while (move.length() != expectedLength) {
            move = scanner.nextLine();

            if (move.length() != expectedLength)
                System.out.println("Wrong move. Type like: b1c3");
        }
        moveUpdated = true;
        return move;
    }

    @Override
    public boolean getMoveUpdated() {
        return this.moveUpdated;
    }

    @Override
    public void resetMoveUpdated() {
        this.moveUpdated = false;
    }
}
