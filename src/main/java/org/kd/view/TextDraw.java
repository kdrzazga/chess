package org.kd.view;

import org.kd.lib.Sout;
import org.kd.model.*;
import org.kd.model.board.Board;
import org.kd.model.chessmen.*;

import java.util.Iterator;

public class TextDraw implements IDrawable {

    @Override
    public void draw(Board board) {
        final String columnCaption = "   a  b  c  d  e  f  g  h";

        printTopInfo(board);
        System.out.println(columnCaption);
        for (int col = Board.COLUMN_SIZE - 1; col >= 0; col--) {
            Sout.println('-', 3 * Board.COLUMN_SIZE + 1);

            for (int row = 0; row < Board.ROW_SIZE; row++) {
                drawBoardField(board, col, row);
            }

            System.out.println("|");
        }
        Sout.println('-', 3 * Board.COLUMN_SIZE + 2);
        System.out.println(columnCaption);
        printBottomInfo(board);
    }

    private void drawBoardField(Board board, int col, int row) {
        if (row == 0) System.out.print(col + 1);
        System.out.print("|");

        Iterator<Chessman> chessmanIterator = board.chessmen.iterator();

        boolean found = false;
        while (chessmanIterator.hasNext()) {
            Chessman chessman = chessmanIterator.next();
            if (chessman.getField().getTableXCoord() == row
                    && chessman.getField().getTableYCoord() == col) {
                System.out.print(Chessman.getTextSymbol(chessman));
                found = true;
            }
        }
        if (!found) System.out.print("  ");
    }

    private void printTopInfo(Board board) {
        System.out.println(
                Chessman.getTextSymbol(Tower.class) + " - Tower, " +
                        Chessman.getTextSymbol(Knight.class) + " - Knight, " +
                        Chessman.getTextSymbol(Bishop.class) + " - Bishop, " +
                        Chessman.getTextSymbol(Queen.class) + " - Queen, " +
                        Chessman.getTextSymbol(King.class) + " - King, " +
                        Chessman.getTextSymbol(Pawn.class) + " - Pawn, " +
                        Chessman.getTextSymbol(Side.WHITE) + " - white, " +
                        Chessman.getTextSymbol(Side.BLACK) + " - black\n"
        );
    }

    private void printBottomInfo(Board board) {
        System.out.print("Beaten: ");
        if (board.beatenChessmen.isEmpty())
            System.out.print("none");
        else
            for (Chessman chessman : board.beatenChessmen) {
                System.out.print(Chessman.getTextSymbol(chessman));
            }
        System.out.println();
        System.out.println("Current turn: " + board.getTurn());
    }
}
