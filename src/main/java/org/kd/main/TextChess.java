package org.kd.main;

import org.kd.model.Game;
import org.kd.view.TextDraw;
import org.kd.view.TextInput;

public class TextChess {

    public static void main(String[] args) {
        Game game = new Game(new TextDraw(), new TextInput());

        System.out.println("\nWelcome to chess. Please type your move");
        game.start();

    }
}
