package org.kd.main;

import org.kd.model.board.Board;
import org.kd.model.board.BoardFactory;
import org.kd.view.TextDraw;

public class TextBoardDraw {

    private final static Board board = BoardFactory.createStartingBoard();

    public static void main(String[] args) {
        TextDraw td = new TextDraw();
        moveKnights();
        movePawns();
        moveBishop();
        td.draw(board);
    }

    private static void movePawns() {
        board.move("d2", "d3");
        board.move("e2", "e4");
    }

    private static void moveKnights() {
        board.move("b1", "a2");//shall not be executed
        board.move("b1", "a3");
        board.move("b8", "c6");
    }

    private static void moveBishop() {
        board.move("b1", "h6"); //shall not be executed
        board.move("c1", "h6");
    }

}
