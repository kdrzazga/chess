package org.kd;

import org.kd.model.IDrawable;
import org.kd.view.TextDraw;

public abstract class TestWithDrawing {

    protected final IDrawable boardDrawer = new TextDraw();

}
