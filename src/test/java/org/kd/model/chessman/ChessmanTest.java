package org.kd.model.chessman;

import org.junit.Test;

public interface ChessmanTest {
    @Test
    void testValidMoves();

    @Test
    void testInvalidMoves();
}
