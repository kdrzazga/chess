package org.kd.model.chessman;

import org.junit.Test;
import org.kd.model.board.BoardBuilder;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.Knight;

import static org.junit.Assert.assertTrue;

public class KnightTests implements ChessmanTest {

    @Override
    @Test
    public void testValidMoves() {
        testValidMovesB8();
        testValidMovesG1();
    }

    private void testValidMovesB8() {
        Knight knightB8 = (Knight) new BoardBuilder().addBlackStartingRow8().build().chessmen.get(1);
        assertTrue(new BoardField("b8").equals(knightB8.getField()));

        BoardField validFields[] = {
                new BoardField("a6"),
                new BoardField("c6"),
                new BoardField("d7")
        };

        for (BoardField field : validFields)
            assertTrue(knightB8.moveCompliesWithRules(field));
    }

    private void testValidMovesG1() {
        Knight knightG1 = (Knight) new BoardBuilder().addWhiteStartingRow1().build().chessmen.get(6);
        assertTrue(new BoardField("g1").equals(knightG1.getField()));

        BoardField validFields[] = {
                new BoardField("e2"),
                new BoardField("f3"),
                new BoardField("h3")
        };

        for (BoardField field : validFields)
            assertTrue(knightG1.moveCompliesWithRules(field));
    }

    @Override
    @Test
    public void testInvalidMoves() {

    }
}
