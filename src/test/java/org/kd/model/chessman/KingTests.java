package org.kd.model.chessman;

import org.junit.Test;
import org.kd.TestWithDrawing;
import org.kd.model.Side;
import org.kd.model.board.Board;
import org.kd.model.board.BoardBuilder;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.King;
import org.kd.model.chessmen.Tower;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class KingTests extends TestWithDrawing {

    @Test
    public void testValidMoves() {
        testNonCastlingMoves();
        testCastlingMoves();
    }

    @Test
    public void testInvalidMoves() {
        King testKing = new King(Side.BLACK);
        BoardField[] invalidLocations = {
                new BoardField("g7"),
                new BoardField("g6"),
                new BoardField("f6"),
                new BoardField("e6"),
                new BoardField("d6"),
                new BoardField("c6"),
                new BoardField("c7"),
                new BoardField("h1")
        };

        for (BoardField field : invalidLocations)
            assertFalse(String.format("Failed for field %s", field), testKing.moveCompliesWithRules(field));
    }

    private void testCastlingMoves() {
        King testKing = new King(Side.BLACK);
        BoardField[] castlingLocations = {
                new BoardField("g8"),
                new BoardField("c8")
        };

        for (BoardField field : castlingLocations)
            assertTrue(String.format("Failed for field %s", field), testKing.moveCompliesWithRules(field));
    }

    private void testNonCastlingMoves() {
        King testKing = new King(Side.WHITE);
        testKing.setField(new BoardField("c3"));
        testKing.setAlreadyMoved();
        BoardField[] availableLocations = {
                new BoardField("c4"),
                new BoardField("d4"),
                new BoardField("d3"),
                new BoardField("d2"),
                new BoardField("c2"),
                new BoardField("b2"),
                new BoardField("b3"),
                new BoardField("b4")
        };

        for (BoardField field : availableLocations)
            assertTrue(String.format("Failed for field %s", field), testKing.moveCompliesWithRules(field));
    }

    @Test
    public void testKingCastlingAfterItAlreadyMoved() {
        Board testBoard = new BoardBuilder().addBlackStartingRow8().build();

        testBoard.changeTurn();
        testBoard.move("g8", "h6");//move knight away
        testBoard.changeTurn();
        testBoard.move("f8", "c5");//and bishop too
        testBoard.changeTurn();
        testBoard.move("e8", "f8");//move king
        testBoard.changeTurn();
        boardDrawer.draw(testBoard);
        testBoard.move("f8", "e8");//and return
        testBoard.changeTurn();
        testBoard.move("e8", "g8");//try castling
        testBoard.changeTurn();

        boardDrawer.draw(testBoard);
        assertTrue(((King)testBoard.getActiveChessmanAt("e8")).getAlreadyMoved());
        assertTrue((testBoard.getActiveChessmanAt("e8").getClass().equals(King.class)));
        assertTrue((testBoard.getActiveChessmanAt("h8").getClass().equals(Tower.class)));
    }
}
