package org.kd.model.chessman;

import org.junit.Test;
import org.kd.model.board.BoardBuilder;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.Queen;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QueenTests implements ChessmanTest {

    @Override
    @Test
    public void testValidMoves() {
        Queen queenD1 = (Queen) new BoardBuilder().addWhiteStartingRow1().build().chessmen.get(3);

        assertTrue(new BoardField("d1").equals(queenD1.getField()));
        for (int row = 2; row <= 8; row++)
            assertTrue(queenD1.moveCompliesWithRules(new BoardField("d".concat(String.valueOf(row)))));

        assertTrue(queenD1.moveCompliesWithRules(new BoardField("a4")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("b3")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("c2")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("e2")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("f3")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("g4")));
        assertTrue(queenD1.moveCompliesWithRules(new BoardField("h5")));
    }

    @Override
    @Test
    public void testInvalidMoves() {
        Queen queenD1 = (Queen) new BoardBuilder().addWhiteStartingRow1().build().chessmen.get(3);

        assertTrue(new BoardField("d1").equals(queenD1.getField()));
        for (int row = 3; row <= 8; row++) {
            assertFalse(queenD1.moveCompliesWithRules(new BoardField("c".concat(String.valueOf(row)))));
            assertFalse(queenD1.moveCompliesWithRules(new BoardField("e".concat(String.valueOf(row)))));
        }

        assertFalse(queenD1.moveCompliesWithRules(new BoardField("c7")));
        assertFalse(queenD1.moveCompliesWithRules(new BoardField("b6")));
        assertFalse(queenD1.moveCompliesWithRules(new BoardField("a5")));
        assertFalse(queenD1.moveCompliesWithRules(new BoardField("e7")));
        assertFalse(queenD1.moveCompliesWithRules(new BoardField("f6")));
        assertFalse(queenD1.moveCompliesWithRules(new BoardField("h4")));
    }
}
