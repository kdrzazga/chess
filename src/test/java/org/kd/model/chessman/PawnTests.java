package org.kd.model.chessman;

import org.junit.Assert;
import org.junit.Test;
import org.kd.TestWithDrawing;
import org.kd.model.Side;
import org.kd.model.board.TestBoardFactory;
import org.kd.model.board.Board;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.NullChessman;
import org.kd.model.chessmen.Pawn;
import org.kd.model.chessmen.PawnFactory;

import static org.junit.Assert.*;

public class PawnTests extends TestWithDrawing {

    @Test
    public void testMoveWhitePawnFromStartLine() {
        Pawn whitePawn = PawnFactory.createWhitesStartingRow().get(2);
        assertTrue(new BoardField("c2").equals(whitePawn.getField()));

        assertTrue(whitePawn.moveCompliesWithRules(new BoardField('c', 3)));
        assertTrue(whitePawn.moveCompliesWithRules(new BoardField('c', 4)));
        assertTrue(whitePawn.moveCompliesWithRules(new BoardField('b', 3)));
        assertTrue(whitePawn.moveCompliesWithRules(new BoardField('d', 3)));
        assertFalse(whitePawn.moveCompliesWithRules(new BoardField('c', 1)));
        assertFalse(whitePawn.moveCompliesWithRules(new BoardField('d', 2)));
    }

    @Test
    public void testMoveBlackPawnFromStartLine() {
        Pawn blackPawn = PawnFactory.createBlacksStartingRow().get(7);
        assertTrue(new BoardField("h7").equals(blackPawn.getField()));

        assertTrue(blackPawn.moveCompliesWithRules(new BoardField('g', 6)));
        assertTrue(blackPawn.moveCompliesWithRules(new BoardField('h', 6)));
        assertTrue(blackPawn.moveCompliesWithRules(new BoardField('h', 5)));
        assertFalse(blackPawn.moveCompliesWithRules(new BoardField('h', 7)));
        assertFalse(blackPawn.moveCompliesWithRules(new BoardField('h', 8)));
    }

    @Test
    public void testPawnsGoingForward() {
        Board board = TestBoardFactory.createOnlyWhiteChessmenBoard();
        board.chessmen.addAll(PawnFactory.createBlacksStartingRow());
        board.move("a6", "a7");//incorrect
        board.move("a6", "b7");
        boardDrawer.draw(board);
        assertEquals(NullChessman.class, board.getActiveChessmanAt("a6").getClass());
        Assert.assertEquals(Side.BLACK, board.getActiveChessmanAt("a7").getSide());
        assertEquals(Side.WHITE, board.getActiveChessmanAt("b7").getSide());

    }

    @Test
    public void testSimpleBlackPawnMove() {
        Board board = TestBoardFactory.createStartingBoard();

        board.move("d2", "d4");

        boardDrawer.draw(board);
        board.move("a7", "a6");
        boardDrawer.draw(board);
    }

    @Test
    public void testPawnBlockingByOpponent(){
        Board board = TestBoardFactory.createPawnBeatAndBlockBoard();
        boardDrawer.draw(board);

        board.move("f2", "f4"); //blocked
        board.move("f2", "f3"); //blocked
        board.move("f2", "g3"); //beat
        boardDrawer.draw(board);

        board.move("f4", "f2"); //blocked
        board.move("f4", "f3"); //blocked
        board.move("f4", "g3"); //beat
        boardDrawer.draw(board);

        assertTrue(Side.BLACK.equals(board.getActiveChessmanAt("f3").getSide()));
        assertTrue(Side.BLACK.equals(board.getActiveChessmanAt("g3").getSide()));
        assertTrue(Side.NONE.equals(board.getActiveChessmanAt("f2").getSide()));
        assertTrue(Side.NONE.equals(board.getActiveChessmanAt("f4").getSide()));
    }

    @Test
    public void testPawnPromotion() {
        throw new RuntimeException("not implemented yet");
    }

}
