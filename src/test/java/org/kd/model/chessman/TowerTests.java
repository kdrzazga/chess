package org.kd.model.chessman;

import org.junit.Test;
import org.kd.TestWithDrawing;
import org.kd.model.Side;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.Tower;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TowerTests extends TestWithDrawing {

    @Test
    public void testValidMoves() {
        Tower testTower = new Tower("d3", Side.WHITE);
        BoardField[] availableLocations = {
                new BoardField("d1"),
                new BoardField("d2"),
                new BoardField("d4"),
                new BoardField("d5"),
                new BoardField("d6"),
                new BoardField("d7"),
                new BoardField("d8"),
                new BoardField("a3"),
                new BoardField("b3"),
                new BoardField("c3"),
                new BoardField("e3"),
                new BoardField("f3"),
                new BoardField("g3"),
                new BoardField("h3"),
        };

        for (BoardField field : availableLocations)
            assertTrue(testTower.moveCompliesWithRules(field));
    }

    @Test
    public void testInvalidMoves() {
        Tower testTower = new Tower("d3", Side.WHITE);
        BoardField[] availableLocations = {
                new BoardField("d3"),
                new BoardField("b2"),
                new BoardField("c4"),
                new BoardField("e5"),
                new BoardField("f6"),
                new BoardField("g7"),
                new BoardField("h8"),
                new BoardField("a7"),
                new BoardField("b8"),
                new BoardField("c6"),
                new BoardField("e7"),
                new BoardField("f5"),
                new BoardField("g2"),
                new BoardField("h1")
        };

        for (BoardField field : availableLocations)
            assertFalse(testTower.moveCompliesWithRules(field));
    }
}
