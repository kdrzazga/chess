package org.kd.model.chessman;

import org.junit.Test;
import org.kd.TestWithDrawing;
import org.kd.model.board.TestBoardFactory;
import org.kd.model.board.Board;
import org.kd.model.board.BoardBuilder;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.Bishop;
import org.kd.model.chessmen.Queen;
import org.kd.model.chessmen.Tower;

import static org.junit.Assert.*;

public class BishopTests extends TestWithDrawing{

    @Test
    public void testValidMoves() {
        Bishop bishopC8 = (Bishop) new BoardBuilder().addBlackStartingRow8().build().chessmen.get(2);

        assertTrue(new BoardField("c8").equals(bishopC8.getField()));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("d7")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("e6")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("f5")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("g4")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("h3")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("b7")));
        assertTrue(bishopC8.moveCompliesWithRules(new BoardField("a6")));
    }

    @Test
    public void testInvalidMoves() {
        Bishop bishopC1 = (Bishop) new BoardBuilder().addWhiteStartingRow1().build().chessmen.get(2);

        assertTrue(new BoardField("c1").equals(bishopC1.getField()));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("b1")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("a2")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("d3")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("e4")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("f5")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("g6")));
        assertFalse(bishopC1.moveCompliesWithRules(new BoardField("h7")));
    }

    @Test
    public void testCombinedMoves() {

        //given
        Board board = TestBoardFactory.createOnlyWhiteChessmenBoard();

        //when
        board.move("a1", "a3");
        board.changeTurn();
        board.move("d1", "d3");
        board.changeTurn();
        board.move("c1", "a3");//incorrect move attempt
        board.move("c1", "b2");
        board.changeTurn();
        board.move("f1", "a6");//incorrect move attempt
        board.move("f1", "b5");//incorrect move attempt
        board.move("f1", "c4");//incorrect move attempt
        board.move("f1", "d3");//incorrect move attempt
        board.move("f1", "e2");
        board.changeTurn();
        board.move("e2", "h5");

        //then
        /*
          a  b   c  d  e  f  g  h
        -------------------------
        1|  |kw|  |  |Kw|  |kw|Tw|
        -------------------------
        2|  |Bw|  |  |  |  |  |  |
        -------------------------
        3|Tw|  |  |Qw|  |  |  |  |
        -------------------------
        4|  |  |  |  |  |  |  |  |
        -------------------------
        5|  |  |  |  |  |  |  |Bw|
        -------------------------
        6|Pw|Pw|Pw|Pw|Pw|Pw|Pw|Pw|
        -------------------------
        7|  |  |  |  |  |  |  |  |
        -------------------------
        8|  |  |  |  |  |  |  |  |
        --------------------------
         */

        boardDrawer.draw(board);

        assertEquals(Bishop.class, board.getActiveChessmanAt("b2").getClass());
        assertEquals(Tower.class, board.getActiveChessmanAt("a3").getClass());
        assertNotEquals("Bishop wasn't expected on a3", Bishop.class, board.getActiveChessmanAt("a3").getClass());
        assertNotEquals("Bishop wasn't expected on a6", Bishop.class, board.getActiveChessmanAt("a6").getClass());
        assertNotEquals("Bishop wasn't expected on e2", Bishop.class, board.getActiveChessmanAt("e2").getClass());
        assertEquals(Queen.class, board.getActiveChessmanAt("d3").getClass());
        assertEquals(Bishop.class, board.getActiveChessmanAt("h5").getClass());
    }
}
