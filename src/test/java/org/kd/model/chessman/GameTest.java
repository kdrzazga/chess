package org.kd.model.chessman;

import org.junit.Assert;
import org.junit.Test;
import org.kd.model.IDrawable;
import org.kd.model.Side;
import org.kd.model.board.Board;
import org.kd.model.board.BoardFactory;
import org.kd.model.chessmen.King;
import org.kd.model.chessmen.Knight;
import org.kd.model.chessmen.Pawn;
import org.kd.model.chessmen.Queen;
import org.kd.view.TextDraw;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GameTest {
    private final IDrawable boardDrawer = new TextDraw();

    @Test
    public void test4moveCheckmate(){
        Board board = BoardFactory.createStartingBoard();

        board.move("e2", "e4");
        board.move("a7", "a6");
        board.move("d1", "f3");
        board.move("h7", "h6");
        board.move("f1", "b5");
        board.move("a6", "a5");
        board.move("f3", "f7");
        boardDrawer.draw(board);

        assertEquals(Queen.class, board.getActiveChessmanAt("f7").getClass());
    }

    @Test
    public void testThreeBeats(){
        Board board = BoardFactory.createStartingBoard();

        board.move("e2", "e4");
        board.move("a7", "a6");
        board.move("d1", "f3");
        board.move("h7", "h6");
        board.move("f3", "f7");
        board.move("e8", "f7");
        board.move("g1", "h3");
        boardDrawer.draw(board);
        assertEquals(Knight.class, board.getActiveChessmanAt("h3").getClass());
        board.move("b8", "c6");
        boardDrawer.draw(board);
        assertEquals(Knight.class, board.getActiveChessmanAt("h3").getClass());
        board.move("h3", "f5");// not performed
        board.move("h6", "f5");//not performed
        boardDrawer.draw(board);

        assertEquals(King.class, board.getActiveChessmanAt("f7").getClass());
        assertNotEquals(Queen.class, board.getActiveChessmanAt("f7").getClass());
        assertNotEquals(Pawn.class, board.getActiveChessmanAt("f7").getClass());
        Assert.assertEquals(Side.BLACK, board.getActiveChessmanAt("f7").getSide());
    }

}
