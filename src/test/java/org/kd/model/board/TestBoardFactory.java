package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.board.Board;
import org.kd.model.board.BoardBuilder;
import org.kd.model.board.BoardFactory;
import org.kd.model.board.BoardField;
import org.kd.model.chessmen.*;

import java.util.Arrays;

public class TestBoardFactory extends BoardFactory {

    private TestBoardFactory() {
        super();
    }

    public static Board createOnlyWhiteChessmenBoard() {
        Board board = new BoardBuilder().addWhiteStartingRow1().addWhitePawns().build();

        board.chessmen.stream()
                .filter(chessman -> chessman.getClass().equals(Pawn.class))
                .forEach(chessman -> chessman.setField(chessman.getField().getTableXCoord(), 5)
                );
        return board;
    }

    /*
          a  b   c  d  e  f  g  h
        -------------------------
        1|  |  |  |  |  |  |  |Bw|
        -------------------------
        2|  |  |  |  |  |  |Kw|  |
        -------------------------
        3|  |  |  |  |Qb|  |  |  |
        -------------------------
        4|  |  |  |  |  |  |  |Tb|
        -------------------------
        5|  |  |  |  |  |  |  |  |
        -------------------------
        6|  |  |  |  |  |  |  |  |
        -------------------------
        7|  |  |  |  |  |  |  |  |
        -------------------------
        8|  |  |  |  |  |Tb|  |  |
        --------------------------
         */

    static Board createStaleMateBoard() {
        Board staleMateBoard = new Board();
        staleMateBoard.changeTurn(); //black's turn now, default was white's

        Queen blackQueen = new Queen(Side.BLACK);
        blackQueen.setField("e3");

        King whiteKing = new King(Side.WHITE);
        whiteKing.setField("g2");

        Chessman boardSetup[] = {
                new Bishop("h1", Side.WHITE),
                whiteKing,
                blackQueen,
                new Pawn("e2", Side.BLACK),
                new Tower("h4", Side.BLACK),
        };

        staleMateBoard.chessmen.addAll(Arrays.asList(boardSetup));

        return staleMateBoard;
    }

    /*
              a  b   c  d  e  f  g  h
            -------------------------
            1|Tw|kw|  |  |Kw|Bw|  |Tw|
            -------------------------
            2|  |  |  |  |Qb|  |  |  |
            -------------------------
            3|  |  |  |  |  |  |  |  |
            -------------------------
            4|  |  |  |kb|  |  |  |  |
            -------------------------
            5|  |  |  |  |  |  |  |  |
            -------------------------
            6|  |  |  |  |  |  |  |  |
            -------------------------
            7|  |  |  |  |  |  |  |  |
            -------------------------
            8|  |  |  |  |  |  |  |  |
            --------------------------
             */
    static Board createCheckMateBoard() {

        Queen blackQueen = new Queen(Side.BLACK);
        blackQueen.setField("e2");

        Board checkMateBoard = setupBoardInitially(blackQueen);

        return checkMateBoard;
    }

    /*
              a  b   c  d  e  f  g  h
            -------------------------
            1|Tw|kw|  |  |Kw|Bw|  |Tw|
            -------------------------
            2|  |  |  |  |  |  |  |  |
            -------------------------
            3|  |  |Qb|  |  |  |  |  |
            -------------------------
            4|  |  |  |kb|  |  |  |  |
            -------------------------
            5|  |  |  |  |  |  |  |  |
            -------------------------
            6|  |  |  |  |  |  |  |  |
            -------------------------
            7|  |  |  |  |  |  |  |  |
            -------------------------
            8|  |  |  |  |  |  |  |  |
            --------------------------
             */
    static Board createCheckBoard() {

        Queen blackQueen = new Queen(Side.BLACK);
        blackQueen.setField("c3");

        Board checkBoard = setupBoardInitially(blackQueen);

        return checkBoard;
    }

    private static Board setupBoardInitially(Queen blackQueen) {
        Board board = new Board();
        board.changeTurn(); //black's turn now, default was white's

        King whiteKing = new King(Side.WHITE);
        whiteKing.setField("e1");

        Chessman boardSetup[] = {
                new Tower("a1", Side.WHITE),
                new Knight("b1", Side.WHITE),
                whiteKing,
                new Bishop("f1", Side.WHITE),
                new Tower("h1", Side.WHITE),
                blackQueen,
                new Knight("d4", Side.WHITE),
        };

        board.chessmen.addAll(Arrays.asList(boardSetup));

        return board;
    }

    public static Board createPawnBeatAndBlockBoard() {
        Board board = new Board();
        board.chessmen.add(new Pawn(new BoardField("f2"), Side.WHITE));
        board.chessmen.add(new Pawn(new BoardField("f3"), Side.BLACK));
        board.chessmen.add(new Pawn(new BoardField("g3"), Side.BLACK));
        board.chessmen.add(new Pawn(new BoardField("f4"), Side.BLACK));

        return board;
    }


}
