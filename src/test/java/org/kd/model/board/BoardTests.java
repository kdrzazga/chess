package org.kd.model.board;

import org.junit.Before;
import org.junit.Test;
import org.kd.lib.ListsCompare;
import org.kd.model.IDrawable;
import org.kd.model.Side;
import org.kd.model.chessmen.*;
import org.kd.view.TextDraw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BoardTests {

    private Board startingBoard;
    private final static boolean enableDrawing = true;
    private final static IDrawable boardDrawer = new TextDraw();

    @Before
    public void initBoard() {
        startingBoard = BoardFactory.createStartingBoard();
    }

    @Test
    public void testVerticalTowerMoves() {
        //given
        Board board = TestBoardFactory.createOnlyWhiteChessmenBoard();

        //when
        board.move("a1", "a8");
        board.move("a1", "a3");
        board.move("b1", "a3");
        board.move("a3", "a7");

        //then
        /*
          a  b   c  d  e  f  g  h
        -------------------------
        1|  |kw|Bw|Qw|Kw|Bw|kw|Tw|
        -------------------------
        2|  |  |  |  |  |  |  |  |
        -------------------------
        3|Tw|  |  |  |  |  |  |  |
        -------------------------
        4|  |  |  |  |  |  |  |  |
        -------------------------
        5|  |  |  |  |  |  |  |  |
        -------------------------
        6Pw|Pw|Pw|Pw|Pw|Pw|Pw|Pw|
        -------------------------
        7|  |  |  |  |  |  |  |  |
        -------------------------
        8|  |  |  |  |  |  |  |  |
        --------------------------
         */

        assertEquals(Tower.class, board.getActiveChessmanAt("a3").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("a1").getClass());
        assertEquals(Knight.class, board.getActiveChessmanAt("b1").getClass());
    }

    @Test
    public void testHorizontalTowerMoves() {

        //given
        Board board = TestBoardFactory.createOnlyWhiteChessmenBoard();

        //when
        board.move("a1", "a4");
        board.changeTurn();
        board.move("c1", "f4");
        board.changeTurn();
        board.move("a4", "e4"); //incorrect move attempt
        board.move("e4", "h4");

        //then
        /*
          a  b   c  d  e  f  g  h
        -------------------------
        1|  |kw|  |Qw|Kw|Bw|kw|Tw|
        -------------------------
        2|  |  |  |  |  |  |  |  |
        -------------------------
        3|  |  |  |  |  |  |  |  |
        -------------------------
        4|  |  |  |  |Tw|Bw|  |  |
        -------------------------
        5|  |  |  |  |  |  |  |  |
        -------------------------
        6Pw|Pw|Pw|Pw|Pw|Pw|Pw|Pw|
        -------------------------
        7|  |  |  |  |  |  |  |  |
        -------------------------
        8|  |  |  |  |  |  |  |  |
        --------------------------
         */

        assertEquals(Tower.class, board.getActiveChessmanAt("e4").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("a1").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("c1").getClass());
        assertEquals(Bishop.class, board.getActiveChessmanAt("f4").getClass());
    }

    @Test
    public void testQueenMoves() {

        //given
        Board board = TestBoardFactory.createOnlyWhiteChessmenBoard();

        //when
        board.move("d1", "d8"); //incorrect move attempt
        board.move("g1", "f3");
        board.changeTurn();
        board.move("d1", "h5"); //incorrect move attempt
        board.move("d1", "d5");

        //then
        /*
          a  b   c  d  e  f  g  h
        -------------------------
        1|Tw|kw|Bw|  |Kw|Bw|  |Tw|
        -------------------------
        2|  |  |  |  |  |  |  |  |
        -------------------------
        3|  |  |  |  |  |kw|  |  |
        -------------------------
        4|  |  |  |  |  |  |  |  |
        -------------------------
        5|  |  |  |Qw|  |  |  |  |
        -------------------------
        6Pw|Pw|Pw|Pw|Pw|Pw|Pw|Pw|
        -------------------------
        7|  |  |  |  |  |  |  |  |
        -------------------------
        8|  |  |  |  |  |  |  |  |
        --------------------------
         */

        assertEquals(NullChessman.class, board.getActiveChessmanAt("d1").getClass());
        assertEquals(Knight.class, board.getActiveChessmanAt("f3").getClass());
        assertNotEquals("Queen wasn't expected on h5", Queen.class, board.getActiveChessmanAt("h5").getClass());
        assertEquals(Queen.class, board.getActiveChessmanAt("d5").getClass());
    }

    @Test
    public void testKingCastlingLeft() {
        Board board = new TestBoardForCastlingBuilder().getBoardForLongCastling().build();
        board.move("e1", "c1");
        if (enableDrawing) boardDrawer.draw(board);

        assertEquals(Tower.class, board.getActiveChessmanAt("d1").getClass());
    }

    @Test
    public void testKingCastlingRight() {
        Board board = new TestBoardForCastlingBuilder().getBoardForShortCastling().build();
        board.move("e1", "g1");
        if (enableDrawing) boardDrawer.draw(board);

        assertEquals(Tower.class, board.getActiveChessmanAt("f1").getClass());
    }

    @Test
    public void testKingCastlingThruCheckedField() {
        Board board = new TestBoardForCastlingBuilder().getBoardForShortCastling().withCheckForShortCastling().build();
        board.move("e1", "g1");
        if (enableDrawing) boardDrawer.draw(board);

        throw new RuntimeException("not implemented yet");
    }

    @Test
    public void testKingCastlingDuringCheck() {
        Board board = new TestBoardForCastlingBuilder().getBoardForShortCastling().getBoardForLongCastling().withCheckFromTower().build();

        board.move("e1", "c1");
        if (enableDrawing) boardDrawer.draw(board);

        assertEquals(King.class, board.getActiveChessmanAt("e1").getClass());
        assertEquals(Tower.class, board.getActiveChessmanAt("a1").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("b1").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("c1").getClass());
        assertEquals(NullChessman.class, board.getActiveChessmanAt("d1").getClass());
    }

    @Test
    public void testTowersOnStartingBoard() {
        BoardField towerFields[] = {
                new BoardField("a1"),
                new BoardField("h1"),
                new BoardField("a8"),
                new BoardField("h8")
        };

        assertTrue(checkChessmanTypeOnFields(Tower.class, towerFields));
    }

    @Test
    public void testKnightsOnStartingBoard() {
        BoardField knightFields[] = {
                new BoardField("b1"),
                new BoardField("g1"),
                new BoardField("g8"),
                new BoardField("b8")
        };

        assertTrue(checkChessmanTypeOnFields(Knight.class, knightFields));
    }

    @Test
    public void testBishopsOnStartingBoard() {
        BoardField bishopFields[] = {
                new BoardField("c1"),
                new BoardField("f1"),
                new BoardField("c8"),
                new BoardField("f8")
        };

        assertTrue(checkChessmanTypeOnFields(Bishop.class, bishopFields));
    }

    @Test
    public void checkPawnsOnStartingBoard() {
        List<BoardField> whitePawnsPositions = new ArrayList<>(8);
        List<BoardField> blackPawnsPositions = new ArrayList<>(8);

        for (int row = 0; row < Board.ROW_SIZE; row++) {
            whitePawnsPositions.add(new BoardField(row, 1));
            blackPawnsPositions.add(new BoardField(row, 6));
        }

        assertTrue(checkChessmanTypeOnFields(Pawn.class, whitePawnsPositions.toArray(new BoardField[whitePawnsPositions.size()])));
        assertTrue(checkChessmanTypeOnFields(Pawn.class, blackPawnsPositions.toArray(new BoardField[blackPawnsPositions.size()])));
        assertTrue(checkChessmanColorOnFields(Side.BLACK, blackPawnsPositions.toArray(new BoardField[blackPawnsPositions.size()])));
        assertTrue(checkChessmanColorOnFields(Side.WHITE, whitePawnsPositions.toArray(new BoardField[whitePawnsPositions.size()])));

    }

    @Test
    public void checkEmptyFieldsOnStartingBoard1() {

        for (int row = 0; row < Board.ROW_SIZE; row++)
            for (int col = 2; col < Board.COLUMN_SIZE - 2; col++) {
                BoardField field = new BoardField(row, col);
                assertEquals("Failed for field " + field, NullChessman.class, startingBoard.getActiveChessmanAt(field).getClass());
                assertEquals("Failed for field " + field, Side.NONE, startingBoard.getActiveChessmanAt(field).getSide());
            }
    }

    @Test
    public void checkEmptyFieldsOnStartingBoard2() {

        List<BoardField> vacantFields = startingBoard.getAllUnoccupiedFields();

        for (int row = 0; row < Board.ROW_SIZE; row++)
            for (int col = 2; col < Board.COLUMN_SIZE - 2; col++) {
                final BoardField boardField = new BoardField(row, col);
                assertTrue("Failed for field " + boardField,
                        vacantFields.stream()
                                .anyMatch(boardField::equals));
            }
    }

    @Test
    public void testSimplePawnMoves() {
        startingBoard = TestBoardFactory.createStartingBoard();
        startingBoard.move("d2", "d4");
        startingBoard.changeTurn();
        startingBoard.move("c2", "c3");
        startingBoard.changeTurn();
        startingBoard.move("c3", "c2"); //reverse move impossible
        startingBoard.move("a7", "a4"); //impossible move

        if (enableDrawing) boardDrawer.draw(startingBoard);

        BoardField expectedPawnsPositions[] = {
                new BoardField("a2"),
                new BoardField("b2"),
                new BoardField("c3"),
                new BoardField("a7")
        };

        BoardField expectedEmptyPositions[] = {
                new BoardField("c2"),
                new BoardField("d2"),
                new BoardField("a4")
        };

        assertTrue(checkChessmanTypeOnFields(Pawn.class, expectedPawnsPositions));
        assertTrue(checkChessmanTypeOnFields(NullChessman.class, expectedEmptyPositions));
    }

    @Test
    public void testGetChessmenPositionsForTowers() {
        Board startingBoard = TestBoardFactory.createStartingBoard();

        List<BoardField> towersPositions = new ArrayList<>(4);

        startingBoard.chessmen.stream()
                .filter(chessman -> chessman.getClass().equals(Tower.class))
                .forEach(chessman -> towersPositions.add(chessman.getField()));

        List<BoardField> expectedTowersPositions = Arrays.asList(new BoardField("a8"),
                new BoardField("h8"),
                new BoardField("a1"),
                new BoardField("h1"));

        assertTrue(ListsCompare.compareBoardFieldLists(expectedTowersPositions, towersPositions));
    }

    @Test
    public void testGetChessmenPositionsForPawns() {
        //when
        Board startingBoard = TestBoardFactory.createStartingBoard();

        List<BoardField> actualWhitePawnsPositions = startingBoard.getChessmenPositions(Pawn.class, Side.WHITE);
        List<BoardField> expectedWhitePawnsPositions = new ArrayList<>(8);
        PawnFactory
                .createWhitesStartingRow()
                .forEach(pawn -> expectedWhitePawnsPositions.add(pawn.getField()));

        List<BoardField> actualBlackPawnsPositions = startingBoard.getChessmenPositions(Pawn.class, Side.BLACK);
        List<BoardField> expectedBlackPawnsPositions = new ArrayList<>(8);
        PawnFactory
                .createBlacksStartingRow()
                .forEach(pawn -> expectedBlackPawnsPositions.add(pawn.getField()));

        //then
        assertTrue(ListsCompare.compareBoardFieldLists(expectedBlackPawnsPositions, actualBlackPawnsPositions));
        assertTrue(ListsCompare.compareBoardFieldLists(expectedWhitePawnsPositions, actualWhitePawnsPositions));
    }

    @Test
    public void testCheck() {
        Board board = TestBoardFactory.createCheckBoard();

        if (enableDrawing) boardDrawer.draw(board);
        throw new RuntimeException("not implemented yet");
    }

    @Test
    public void testCheckMate() {
        Board board = TestBoardFactory.createCheckMateBoard();

        if (enableDrawing) boardDrawer.draw(board);
        throw new RuntimeException("not implemented yet");
    }

    @Test
    public void testStaleMate() {
        Board board = TestBoardFactory.createStaleMateBoard();

        if (enableDrawing) boardDrawer.draw(board);
        throw new RuntimeException("not implemented yet");
    }

    private boolean checkChessmanTypeOnFields(Class<? extends Chessman> chessmanType, BoardField fields[]) {
        for (BoardField field : fields) {
            if (!startingBoard.getActiveChessmanAt(field).getClass().equals(chessmanType))
                return false;
        }
        return true;
    }

    private boolean checkChessmanColorOnFields(Side side, BoardField fields[]) {
        for (BoardField field : fields) {
            if (!startingBoard.getActiveChessmanAt(field).getSide().equals(side))
                return false;
        }
        return true;
    }

}
