package org.kd.model.board;

import org.junit.Test;
import org.kd.TestWithDrawing;
import org.kd.model.IDrawable;
import org.kd.model.Side;
import org.kd.view.TextDraw;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AfterMoveAnalyzerTests extends TestWithDrawing {

    private final static boolean enableDrawing = true;
    private final static IDrawable boardDrawer = new TextDraw();

    @Test
    public void testCanKingBeBeatenPositive1() {
        String destFields[] = {"f1", "f2", "f3", "g1", "g3", "h2"};

        for (String dest : destFields) {
            Board board = TestBoardFactory.createStaleMateBoard();
            if (!board.getTurn().equals(Side.WHITE))
                board.changeTurn();

            if (enableDrawing) boardDrawer.draw(board);

            assertTrue("Fail for move g2 ->".concat(dest.toString())
                    , AfterMoveAnalyzer.canKingBeBeaten(
                            board
                            , new BoardField("g2"), new BoardField(dest)));
        }
    }

    @Test
    public void testCanKingBeBeatenNegative1() {
        testKingBeatNegative(new String[]{"d1", "d2"}, new TestBoardForCastlingBuilder()
                .getBoardForLongCastling()
                .withCheckForLongCastling()
                .getBoardForShortCastling()
                .withCheckForShortCastling()
                .build());
    }

    @Test
    public void testCanKingBeBeatenNegative2() {
        testKingBeatNegative(new String[]{"d2", "e2", "f2"}, new BoardBuilder()
                .addWhiteStartingRow1()
                .build());
    }

    private void testKingBeatNegative(String fields[], Board board) {

        for (String dest : fields) {
            if (!board.getTurn().equals(Side.WHITE))
                board.changeTurn();

            if (enableDrawing) boardDrawer.draw(board);

            String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();

            assertFalse(methodName + " Fail for move e1 ->".concat(dest.toString())
                    , AfterMoveAnalyzer.canKingBeBeaten(
                            board
                            , new BoardField("e1"), new BoardField(dest)));
        }
    }
}
