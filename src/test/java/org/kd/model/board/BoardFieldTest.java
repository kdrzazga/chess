package org.kd.model.board;

import org.junit.Assert;
import org.junit.Test;
import org.kd.model.board.BoardField;

public class BoardFieldTest {

    @Test
    public void testBoardCoordinates1(){
        BoardField b4 = new BoardField("b4");

        Assert.assertEquals('b', b4.getBoardXCoord());
        Assert.assertEquals(4, b4.getBoardYCoord());
    }

    @Test
    public void testBoardCoordinates2(){
        BoardField b4 = new BoardField('c', 8);

        b4.setBoardXCoord('h');
        b4.setBoardYCoord(2);

        Assert.assertEquals('h', b4.getBoardXCoord());
        Assert.assertEquals(2, b4.getBoardYCoord());
    }

    @Test
    public void testTableToBoardCoordinatesConversion2(){
        BoardField g7 = new BoardField("g7");

        g7.setTableXCoord(0);
        Assert.assertEquals('a', g7.getBoardXCoord());
    }

}
