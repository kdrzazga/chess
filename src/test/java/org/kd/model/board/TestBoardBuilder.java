package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.*;

import java.util.Arrays;

public class TestBoardBuilder {

    private Board board;


    public TestBoardBuilder(){
        this.board = new Board();
        board.changeTurn(); //black's turn now, default was white's

        King whiteKing = new King(Side.WHITE);
        whiteKing.setField("e1");

        Chessman boardSetup[] = {
                new Tower("a1", Side.WHITE),
                new Knight("b1", Side.WHITE),
                whiteKing,
                new Bishop("f1", Side.WHITE),
                new Tower("h1", Side.WHITE),
                new Knight("d4", Side.WHITE),
        };

        board.chessmen.addAll(Arrays.asList(boardSetup));
    }

    public TestBoardBuilder withCheck(){

        return this;
    }

    public TestBoardBuilder withCheckMate(){
        Queen blackQueen = new Queen(Side.BLACK);
        blackQueen.setField("c3");
        board.chessmen.add(blackQueen);

        return this.withCheck();
    }



    public Board build(){
        return this.board;
    }
}
