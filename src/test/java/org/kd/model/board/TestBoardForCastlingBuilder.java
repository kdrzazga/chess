package org.kd.model.board;

import org.kd.model.Side;
import org.kd.model.chessmen.Tower;

public class TestBoardForCastlingBuilder {

    private Board board;

    public TestBoardForCastlingBuilder() {
        board = new BoardBuilder().addWhiteStartingRow1().build();
    }

    public TestBoardForCastlingBuilder getBoardForShortCastling() {
        this.board.move("g1", "h3");//remove right knight
        this.board.changeTurn();
        this.board.move("f1", "a6");//remove right bishop
        this.board.changeTurn();
        return this;
    }

    public TestBoardForCastlingBuilder getBoardForLongCastling() {
        this.board.move("b1", "a3");//remove left knight
        this.board.changeTurn();
        this.board.move("c1", "h6");//remove left bishop
        this.board.changeTurn();
        this.board.move("d1", "d2");//remove queen
        this.board.changeTurn();
        return this;
    }

    public TestBoardForCastlingBuilder withCheckForShortCastling() {
        this.board.chessmen.add(new Tower(new BoardField("f8"), Side.BLACK));
        return this;
    }

    public TestBoardForCastlingBuilder withCheckForLongCastling() {
        this.board.chessmen.add(new Tower(new BoardField("c8"), Side.BLACK));
        return this;
    }

    public TestBoardForCastlingBuilder withKingAlreadyMoved() {
        this.board.move("e1", "e2");//move King forward
        this.board.move("e2", "e1");//and return
        return this;
    }

    public TestBoardForCastlingBuilder withCheckFromTower() {
        this.board.chessmen.add(new Tower(new BoardField("e8"), Side.BLACK));
        return this;
    }

    public TestBoardForCastlingBuilder withTowerCheckingShortCastling() {
        this.board.chessmen.add(new Tower(new BoardField("f8"), Side.BLACK));
        return this;
    }

    public TestBoardForCastlingBuilder withTowerCheckingLongCastling() {
        this.board.chessmen.add(new Tower(new BoardField("d8"), Side.BLACK));
        return this;
    }

    public Board build() {
        return this.board;
    }
}
