package org.kd.model.board;

import org.junit.Test;
import org.kd.model.board.Board;
import org.kd.model.board.BoardFactory;

import static org.junit.Assert.assertEquals;

public class BoardFactoryTests {

    @Test
    public void testStartingBoardCreation(){
        Board startingBoard = BoardFactory.createStartingBoard();

        assertEquals(32, startingBoard.chessmen.size());
    }

}
