package org.kd.controller.main;

import org.junit.Test;
import org.kd.controller.InputParser;
import org.kd.model.board.BoardField;

import java.text.ParseException;

import static org.junit.Assert.assertTrue;

public class InputParserTest {

    @Test
    public void testInputParser1() {
        InputParser parser = new InputParser();

        try {
            assertTrue(new BoardField("h8").equals(
                    parser.parseDestination("a1h8")));

            assertTrue(new BoardField("a1").equals(
                    parser.parseSource("a1h8")));

        } catch (ParseException pex) {
            System.err.println(pex.getMessage());
        }
    }

    @Test(expected = RuntimeException.class)
    public void testInputParser2() {
        InputParser parser = new InputParser();
        try {
            assertTrue(new BoardField("h9").equals(
                    parser.parseDestination("a1h8")));

        } catch (ParseException pex) {
            System.err.println(pex.getMessage());
        }

    }

    @Test(expected = ParseException.class)
    public void testParsing() throws ParseException {
        InputParser parser = new InputParser();
        parser.parseDestination("Litwo, Ojczyzno moja");
    }
}
